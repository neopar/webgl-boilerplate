import React, { Component } from 'react';
import './App.css';
import Canvas from './Canvas/Canvas';

class App extends Component {
	render() {
		return (
			<div className="App">
				<h1>Simple WebGL App</h1>
        <Canvas />
			</div>
		);
	}
}

export default App;
