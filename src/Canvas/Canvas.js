import React, { Component } from 'react';

class Canvas extends Component {
	componentDidMount() {
		const canvas = this.refs.glCanvas;

		const gl = canvas.getContext('webgl');

		if (gl === null) {
			console.log('Unable to initialize WebGL. Your browser or machine may not support it.');
			alert('Unable to initialize WebGL. Your browser or machine may not support it.');
			return;
		}

		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT);

		const triangle = [
			0, 1, 0,
			1, -1, 0,
			-1, -1, 0
		];

		const colors = [
			1, 0, 0,
			0, 1, 0, 
			0, 0, 1
		];

		const posBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangle), gl.STATIC_DRAW);

		const colorBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

		const vertexShader = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vertexShader, `
		precision mediump float;
		
		attribute vec3 position;
		attribute vec3 color;
		varying  vec3 vColor;

		void main() {
			vColor = color;
			gl_Position = vec4(position, 1);
		}
		`);
		gl.compileShader(vertexShader);

		const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fragmentShader, `
		precision mediump float;
		
		varying  vec3 vColor;

		void main() {
			gl_FragColor = vec4(vColor, 1);
		}
		`);
		gl.compileShader(fragmentShader);

		const program = gl.createProgram();
		gl.attachShader(program, vertexShader);
		gl.attachShader(program, fragmentShader);
		gl.linkProgram(program);

		const posLocation = gl.getAttribLocation(program, 'position');
		gl.enableVertexAttribArray(posLocation);
		gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);
		gl.vertexAttribPointer(posLocation, 3, gl.FLOAT, false, 0, 0);

		const colorLocation = gl.getAttribLocation(program, 'color');
		gl.enableVertexAttribArray(colorLocation);
		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		gl.vertexAttribPointer(colorLocation, 3, gl.FLOAT, false, 0, 0);

		gl.useProgram(program);
		gl.drawArrays(gl.TRIANGLES, 0, 3);
	}

	render() {
		return <canvas ref="glCanvas" width="640" height="480"></canvas>;
	}
}

export default Canvas;
